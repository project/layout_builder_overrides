<?php

namespace Drupal\layout_builder_overrides\Plugin\SectionStorage;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\layout_builder\DefaultsSectionStorageInterface;
use Drupal\layout_builder\OverridesSectionStorageInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\SectionStorage\SectionStorageDefinition;
use Drupal\layout_builder_overrides\Entity\LayoutBuilderEntityViewDisplay;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\EntityContext;

/**
 * Defines the 'overrides' section storage type.
 *
 * @SectionStorage(
 *   id = "view_mode_overrides",
 *   weight = -21,
 *   handles_permission_check = TRUE,
 *   deriver =
 *   "Drupal\layout_builder_overrides\Plugin\SectionStorage\Derivative\ViewModeOverridesSectionStorage",
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity"),
 *     "view_mode" = @ContextDefinition("string"),
 *   },
 * )
 */
class ViewModeOverridesSectionStorage extends OverridesSectionStorage {

  /**
   * {@inheritdoc}
   */
  protected function handleTranslationAccess(AccessResult $result, $operation, AccountInterface $account) {
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRoutes(RouteCollection $collection) {
    $view_mode = $this->getPluginDefinition()
      ->getContextDefinition('view_mode')
      ->getDefaultValue();
    foreach ($this->getEntityTypes() as $entity_type_id => $entity_type) {
      // If the canonical route does not exist, do not provide any Layout
      // Builder UI routes for this entity type.
      if (!$collection->get("entity.$entity_type_id.canonical")) {
        continue;
      }
      $vm = $this->entityTypeManager->getStorage('entity_view_mode')
        ->load($entity_type_id . '.' . $view_mode);
      if ($vm === NULL) {
        continue;
      }

      $defaults = [];
      $defaults['entity_type_id'] = $entity_type_id;

      // Retrieve the requirements from the canonical route.
      $requirements = $collection->get("entity.$entity_type_id.canonical")
        ->getRequirements();

      $options = [];
      // Ensure that upcasting is run in the correct order.
      $options['parameters']['section_storage'] = [];
      $options['parameters'][$entity_type_id]['type'] = 'entity:' . $entity_type_id;

      $template = $entity_type->getLinkTemplate('canonical') . '/layout';
      $this->buildViewModeLayoutRoutes($collection, $this->getPluginDefinition(), $template, $defaults, $requirements, $options, $entity_type_id, $entity_type_id, $view_mode);
    }
  }

  /**
   * Builds the layout routes for the given values.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageDefinition $definition
   *   The definition of the section storage.
   * @param string $path
   *   The path patten for the routes.
   * @param array $defaults
   *   (optional) An array of default parameter values.
   * @param array $requirements
   *   (optional) An array of requirements for parameters.
   * @param array $options
   *   (optional) An array of options.
   * @param string $route_name_prefix
   *   (optional) The prefix to use for the route name.
   * @param string $entity_type_id
   *   (optional) The entity type ID, if available.
   */
  protected function buildViewModeLayoutRoutes(RouteCollection $collection, SectionStorageDefinition $definition, $path, array $defaults = [], array $requirements = [], array $options = [], $route_name_prefix = '', $entity_type_id = '', $view_mode = NULL) {
    $path = $path . '/' . $view_mode;
    $type = $definition->id();
    $defaults['section_storage_type'] = $type;
    // Provide an empty value to allow the section storage to be upcast.
    $defaults['section_storage'] = '';
    // Trigger the layout builder access check.
    $requirements['_layout_builder_access'] = 'view';
    // Trigger the layout builder RouteEnhancer.
    $options['_layout_builder'] = TRUE;
    // Trigger the layout builder param converter.
    $parameters['section_storage']['layout_builder_tempstore'] = TRUE;
    // Merge the passed in options in after Layout Builder's parameters.
    $options = NestedArray::mergeDeep(['parameters' => $parameters], $options);

    if ($route_name_prefix) {
      $route_name_prefix = "layout_builder.$type.$route_name_prefix";
    }
    else {
      $route_name_prefix = "layout_builder.$type";
    }

    $main_defaults = $defaults;
    $main_options = $options;
    $main_defaults['_entity_form'] = "$entity_type_id.layout_builder_overrides";
    $main_defaults['_title_callback'] = '\Drupal\layout_builder\Controller\LayoutBuilderController::title';
    $main_defaults['view_mode'] = $view_mode;
    $route = (new Route($path))
      ->setDefaults($main_defaults)
      ->setRequirements($requirements)
      ->setOptions($main_options);
    $collection->add("$route_name_prefix.view.$view_mode", $route);

    $discard_changes_defaults = $defaults;
    $discard_changes_defaults['_form'] = '\Drupal\layout_builder\Form\DiscardLayoutChangesForm';
    $route = (new Route("$path/discard-changes"))
      ->setDefaults($discard_changes_defaults)
      ->setRequirements($requirements)
      ->setOptions($options);
    $collection->add("$route_name_prefix.discard_changes.$view_mode", $route);

    if (is_subclass_of($definition->getClass(), OverridesSectionStorageInterface::class)) {
      $revert_defaults = $defaults;
      $revert_defaults['_form'] = '\Drupal\layout_builder\Form\RevertOverridesForm';
      $route = (new Route("$path/revert"))
        ->setDefaults($revert_defaults)
        ->setRequirements($requirements)
        ->setOptions($options);
      $collection->add("$route_name_prefix.revert.$view_mode", $route);
    }
    elseif (is_subclass_of($definition->getClass(), DefaultsSectionStorageInterface::class)) {
      $disable_defaults = $defaults;
      $disable_defaults['_form'] = '\Drupal\layout_builder\Form\LayoutBuilderDisableForm';
      $disable_options = $options;
      unset($disable_options['_admin_route'], $disable_options['_layout_builder']);
      $route = (new Route("$path/disable"))
        ->setDefaults($disable_defaults)
        ->setRequirements($requirements)
        ->setOptions($disable_options);
      $collection->add("$route_name_prefix.disable.$view_mode", $route);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildLocalTasks($base_plugin_definition) {
    $local_tasks = [];
    foreach ($this->getEntityTypes() as $entity_type_id => $entity_type) {
      $view_modes = $this->entityTypeManager->getStorage('entity_view_mode')
        ->loadByProperties(['targetEntityType' => $entity_type_id]);
      $bundles = \Drupal::service('entity_type.bundle.info')
        ->getBundleInfo($entity_type_id);
      foreach ($bundles as $bundle_id => $bundle) {
        $view_displays = $this->entityTypeManager->getStorage('entity_view_display')
          ->loadByProperties([
            'targetEntityType' => $entity_type_id,
            'bundle' => $bundle_id,
          ]);
        foreach ($view_displays as $id => $view_display) {
          /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $view_display */
          $view_mode_id = $view_display->getMode();

          if ($this->getPluginDefinition()->getContextDefinition('view_mode')->getDefaultValue() !== $view_mode_id) {
            // Plugin does not apply to this view mode.
            continue;
          }

          $key = $entity_type_id . '.' . $view_mode_id;
          if (!isset($view_modes[$key])) {
            // View mode does not have a config entity.
            continue;
          }

          $view_mode = $view_modes[$key];
          $field_name = static::FIELD_NAME . '_' . $view_mode_id;
          $fields = \Drupal::service('entity_field.manager')
            ->getFieldDefinitions($entity_type_id, $bundle_id);
          if (empty($fields[$field_name])) {
            continue;
          }
          $type = $this->getPluginDefinition()->id();
          $local_tasks["layout_builder.$type.$entity_type_id.view.$view_mode_id"] = $base_plugin_definition + [
              'route_name' => "layout_builder.$type.$entity_type_id.view.$view_mode_id",
              'weight' => 16,
              'title' => $this->t('Layout (@view_mode)', ['@view_mode' => $view_mode->label()]),
              'base_route' => "entity.$entity_type_id.canonical",
              'cache_contexts' => ['layout_builder_is_active:' . $entity_type_id],
            ];
        }
      }
    }
    return $local_tasks;
  }

  protected function getLayoutFieldName() {
    $view_mode = $this->getContextValue('view_mode');
    $field_name = static::FIELD_NAME . '_' . $view_mode;
    return $field_name;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSectionList() {
    $field_name = $this->getLayoutFieldName();
    return $this->getEntity()->get($field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getSectionListFromId($id) {
    @trigger_error('\Drupal\layout_builder\SectionStorageInterface::getSectionListFromId() is deprecated in Drupal 8.7.0 and will be removed before Drupal 9.0.0. The section list should be derived from context. See https://www.drupal.org/node/3016262.', E_USER_DEPRECATED);
    if (strpos($id, '.') !== FALSE) {
      list($entity_type_id, $entity_id) = explode('.', $id, 2);
      $entity = $this->entityRepository->getActive($entity_type_id, $entity_id);
      $field_name = $this->getLayoutFieldName();
      if ($entity instanceof FieldableEntityInterface && $entity->hasField($field_name)) {
        return $entity->get($field_name);
      }
    }
    throw new \InvalidArgumentException(sprintf('The "%s" ID for the "%s" section storage type is invalid', $id, $this->getStorageType()));
  }

  /**
   * {@inheritdoc}
   */
  public function deriveContextsFromRoute($value, $definition, $name, array $defaults) {
    $contexts = [];

    if ($entity = $this->extractEntityFromRoute($value, $defaults)) {
      $contexts['entity'] = EntityContext::fromEntity($entity);
      $view_mode = !empty($defaults['view_mode']) ? $defaults['view_mode'] : $this->getPluginDefinition()->getContextDefinition('view_mode')->getDefaultValue();
      // Retrieve the actual view mode from the returned view display as the
      // requested view mode may not exist and a fallback will be used.
      $view_mode = LayoutBuilderEntityViewDisplay::collectRenderDisplay($entity, $view_mode)
        ->getMode();
      $contexts['view_mode'] = new Context(new ContextDefinition('string'), $view_mode);
    }
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  private function extractEntityFromRoute($value, array $defaults) {
    if (strpos($value, '.') !== FALSE) {
      list($entity_type_id, $entity_id) = explode('.', $value, 2);
    }
    elseif (isset($defaults['entity_type_id']) && !empty($defaults[$defaults['entity_type_id']])) {
      $entity_type_id = $defaults['entity_type_id'];
      $entity_id = $defaults[$entity_type_id];
    }
    else {
      return NULL;
    }

    $view_mode = !empty($defaults['view_mode']) ? $defaults['view_mode'] : $this->getPluginDefinition()->getContextDefinition('view_mode')->getDefaultValue();
    $entity = $this->entityRepository->getActive($entity_type_id, $entity_id);
    $field_name = static::FIELD_NAME . '_' . $view_mode;
    if ($entity instanceof FieldableEntityInterface && $entity->hasField($field_name)) {
      return $entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(RefinableCacheableDependencyInterface $cacheability) {
    $view_mode = $this->getContextValue('view_mode');
    if ($view_mode === 'default' || $view_mode === 'full') {
      return FALSE;
    }
    if ($this->getPluginDefinition()
        ->getContextDefinition('view_mode')
        ->getDefaultValue() !== $view_mode) {
      return FALSE;
    }
    $result = parent::isApplicable($cacheability);
    $field_name = $this->getLayoutFieldName();
    return $this->getEntity()->hasField($field_name) && $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutBuilderUrl($rel = 'view') {
    $view_mode = $this->getContextValue('view_mode');
    if ($view_mode === 'default' || $view_mode === 'full') {
      return parent::getLayoutBuilderUrl($rel);
    }
    $entity = $this->getEntity();
    $route_parameters[$entity->getEntityTypeId()] = $entity->id();
    return Url::fromRoute("layout_builder.{$this->getStorageType()}.{$this->getEntity()->getEntityTypeId()}.$rel.$view_mode", $route_parameters);
  }

}
