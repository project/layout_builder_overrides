<?php

namespace Drupal\layout_builder_overrides\Plugin\SectionStorage\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides section storage per entity view mode.
 *
 * @see \Drupal\layout_builder_overrides\Plugin\SectionStorage\ViewModeOverridesSectionStorage
 */
class ViewModeOverridesSectionStorage extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs new SystemMenuBlock.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    /** @var \Drupal\layout_builder\SectionStorage\SectionStorageDefinition $base_plugin_definition */
    $view_displays = $this->entityTypeManager->getStorage('entity_view_display')
      ->loadMultiple(NULL);
    foreach ($view_displays as $id => $view_display) {
      $view_mode = $view_display->getMode();
      if ($view_mode === 'full' || $view_mode === 'default' || !$view_display->isOverridable()) {
        continue;
      }
      $new_plugin_definition = unserialize(serialize($base_plugin_definition));
      $context_definitions = $new_plugin_definition->getContextDefinitions();
      $context_definitions['entity']->setConstraints(['EntityHasField' => OverridesSectionStorage::FIELD_NAME . '_' . $view_mode]);
      $context_definitions['view_mode']->setDefaultValue($view_mode);
      $new_plugin_definition->addContextDefinition('entity', $context_definitions['entity']);
      $new_plugin_definition->addContextDefinition('view_mode', $context_definitions['view_mode']);
      $new_plugin_definition->set('id', "view_mode_overrides:$view_mode");
      $this->derivatives[$view_mode] = $new_plugin_definition;
    }
    return $this->derivatives;
  }

}
