<?php

namespace Drupal\layout_builder_overrides\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay as CoreLayoutBuilderEntityViewDisplay;

/**
 * Provides an entity view display entity that has a layout.
 */
class LayoutBuilderEntityViewDisplay extends CoreLayoutBuilderEntityViewDisplay {

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if ($this->getMode() === 'default' || $this->getMode() === 'full') {
      return;
    }

    $field_name = OverridesSectionStorage::FIELD_NAME . '_' . $this->getMode();
    $original_value = isset($this->original) ? $this->original->isOverridable() : FALSE;
    $new_value = $this->isOverridable();
    if ($original_value !== $new_value) {
      $entity_type_id = $this->getTargetEntityTypeId();
      $bundle = $this->getTargetBundle();

      if ($new_value) {
        $this->addSectionField($entity_type_id, $bundle, $field_name);
      }
      else {
        $this->removeSectionField($entity_type_id, $bundle, $field_name);
      }
    }

    $already_enabled = isset($this->original) ? $this->original->isLayoutBuilderEnabled() : FALSE;
    $set_enabled = $this->isLayoutBuilderEnabled();
    if ($already_enabled !== $set_enabled) {
      if ($set_enabled) {
        // Loop through all existing field-based components and add them as
        // section-based components.
        $components = $this->getComponents();
        // Sort the components by weight.
        uasort($components, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
        foreach ($components as $name => $component) {
          $this->setComponent($name, $component);
        }
      }
      else {
        // When being disabled, remove all existing section data.
        $this->removeAllSections();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function addSectionField($entity_type_id, $bundle, $field_name) {
    $field = FieldConfig::loadByName($entity_type_id, $bundle, $field_name);
    if (!$field) {
      $translatable = $this->isTranslatable();
      $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);
      if (!$field_storage) {
        $field_storage = FieldStorageConfig::create([
          'entity_type' => $entity_type_id,
          'field_name' => $field_name,
          'type' => 'layout_section',
          'locked' => TRUE,
        ]);
        $field_storage->setTranslatable($translatable);
        $field_storage->save();
      }

      $field = FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => $bundle,
        'label' => t('Layout'),
      ]);
      $field->setTranslatable($translatable);
      $field->save();
    }
  }


  /**
   * Determine if the field must be translatable or not.
   *
   * @return bool
   */
  protected function isTranslatable() {
    $module_handler = \Drupal::moduleHandler();
    $at = $module_handler->moduleExists('layout_builder_at');
    $st = $module_handler->moduleExists('layout_builder_st');
    return $at || $st;
  }

}
