<?php

namespace Drupal\layout_builder_overrides\SectionStorage;

use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\layout_builder_overrides\ContainerDerivativeDiscoveryDecorator;
use Drupal\layout_builder\SectionStorage\SectionStorageManager as CoreSectionStorageManager;

/**
 * {@inheritdoc}
 */
class SectionStorageManager extends CoreSectionStorageManager {

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!$this->discovery) {
      $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
    }
    return $this->discovery;
  }

}
