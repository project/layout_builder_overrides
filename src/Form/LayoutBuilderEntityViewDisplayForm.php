<?php

namespace Drupal\layout_builder_overrides\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Entity\LayoutEntityDisplayInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Form\LayoutBuilderEntityViewDisplayForm as CoreLayoutBuilderEntityViewDisplayForm;

/**
 * Edit form for the LayoutBuilderEntityViewDisplay entity type.
 *
 * @internal
 *   Form classes are internal.
 */
class LayoutBuilderEntityViewDisplayForm extends CoreLayoutBuilderEntityViewDisplayForm {

  public function getLayoutFieldName() {
    return OverridesSectionStorage::FIELD_NAME . '_' . $this->entity->getMode();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    if ($this->entity->getMode() === 'default' || $this->entity->getMode() === 'full') {
      return $form;
    }

    $field_name = $this->getLayoutFieldName();
    // Remove the Layout Builder field from the list.
    $form['#fields'] = array_diff($form['#fields'], [$field_name]);
    unset($form['fields'][$field_name]);

    $is_enabled = $this->entity->isLayoutBuilderEnabled();
    if ($is_enabled) {
      // Hide the table of fields.
      $form['fields']['#access'] = FALSE;
      $form['#fields'] = [];
      $form['#extra'] = [];
    }

    $form['manage_layout'] = [
      '#type' => 'link',
      '#title' => $this->t('Manage layout'),
      '#weight' => -10,
      '#attributes' => ['class' => ['button']],
      '#url' => $this->sectionStorage->getLayoutBuilderUrl(),
      '#access' => $is_enabled,
    ];

    $form['layout'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Layout options'),
      '#tree' => TRUE,
    ];

    $form['layout']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Layout Builder'),
      '#default_value' => $is_enabled,
    ];
    $form['#entity_builders']['layout_builder'] = '::entityFormEntityBuild';

    // @todo Expand to work for all view modes in
    //   https://www.drupal.org/node/2907413.
    $entity_type = $this->entityTypeManager->getDefinition($this->entity->getTargetEntityTypeId());
    $form['layout']['allow_custom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow each @entity to have its layout customized.', [
        '@entity' => $entity_type->getSingularLabel(),
      ]),
      '#default_value' => $this->entity->isOverridable(),
      '#states' => [
        'disabled' => [
          ':input[name="layout[enabled]"]' => ['checked' => FALSE],
        ],
        'invisible' => [
          ':input[name="layout[enabled]"]' => ['checked' => FALSE],
        ],
      ],
    ];
    if (!$is_enabled) {
      $form['layout']['allow_custom']['#attributes']['disabled'] = 'disabled';
    }
    // Prevent turning off overrides while any exist.
    if ($this->hasOverrides($this->entity)) {
      $form['layout']['enabled']['#disabled'] = TRUE;
      $form['layout']['enabled']['#description'] = $this->t('You must revert all customized layouts of this display before you can disable this option.');
      $form['layout']['allow_custom']['#disabled'] = TRUE;
      $form['layout']['allow_custom']['#description'] = $this->t('You must revert all customized layouts of this display before you can disable this option.');
      unset($form['layout']['allow_custom']['#states']);
      unset($form['#entity_builders']['layout_builder']);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function hasOverrides(LayoutEntityDisplayInterface $display) {
    if ($this->entity->getMode() === 'default' || $this->entity->getMode() === 'full') {
      return parent::hasOverrides($display);
    }
    $field_definitions = $this->getFieldDefinitions();
    $field_name = $this->getLayoutFieldName();
    if (empty($field_definitions[$field_name])) {
      return FALSE;
    }
    $entity_type = $this->entityTypeManager->getDefinition($display->getTargetEntityTypeId());
    $query = $this->entityTypeManager->getStorage($display->getTargetEntityTypeId())
      ->getQuery()
      ->exists($field_name);
    if ($bundle_key = $entity_type->getKey('bundle')) {
      $query->condition($bundle_key, $display->getTargetBundle());
    }
    return (bool) $query->count()->execute();
  }

}
