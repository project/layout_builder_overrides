<?php

namespace Drupal\layout_builder_overrides\Form;

use Drupal\layout_builder\Form\OverridesEntityForm;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Provides a form containing the Layout Builder UI for overrides.
 *
 * @internal
 *   Form classes are internal.
 */
class ViewModeOverridesEntityForm extends OverridesEntityForm {

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return $this->getEntity()
        ->getEntityTypeId() . '_layout_builder_overrides_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function init(FormStateInterface $form_state) {
    parent::init($form_state);
    $form_display = EntityFormDisplay::collectRenderDisplay($this->entity, $this->getOperation(), FALSE);
    $form_display->removeComponent(OverridesSectionStorage::FIELD_NAME);
    $field_name = $this->getLayoutFieldName();
    $form_display->setComponent($field_name, [
      'type' => 'layout_builder_widget',
      'weight' => -10,
      'settings' => [],
    ]);

    $this->setFormDisplay($form_display, $form_state);
  }

  public function getLayoutFieldName() {
    $view_mode = $this->getSectionStorage()->getContextValue('view_mode');
    return OverridesSectionStorage::FIELD_NAME . '_' . $view_mode;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL) {
    $this->sectionStorage = $section_storage;
    $context = $section_storage->getContextValues();
    $form = parent::buildForm($form, $form_state, $section_storage);
    unset($form[OverridesSectionStorage::FIELD_NAME]);

    $field_name = $this->getLayoutFieldName();
    $form[$field_name]['#access'] = TRUE;

    return $form;
  }

}
