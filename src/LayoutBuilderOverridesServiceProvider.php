<?php

namespace Drupal\layout_builder_overrides;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\layout_builder_overrides\SectionStorage\SectionStorageManager;

/**
 * Replace the resource type repository for our own configurable version.
 */
class LayoutBuilderOverridesServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('plugin.manager.layout_builder.section_storage');
    $definition->setClass(SectionStorageManager::class);
  }

}
