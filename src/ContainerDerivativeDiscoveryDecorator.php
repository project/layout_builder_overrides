<?php

namespace Drupal\layout_builder_overrides;

use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator as CoreContainerDerivativeDiscoveryDecorator;

/**
 * Injects dependencies into derivers if they use ContainerDeriverInterface.
 *
 * @see \Drupal\Core\Plugin\Discovery\ContainerDeriverInterface
 */
class ContainerDerivativeDiscoveryDecorator extends CoreContainerDerivativeDiscoveryDecorator {

  /**
   * {@inheritdoc}
   */
  protected function getDeriverClass($base_definition) {
    $class = parent::getDeriverClass($base_definition);
    if(empty($class)){
      $class = $base_definition->get('deriver');
    }
    return $class;
  }

}
